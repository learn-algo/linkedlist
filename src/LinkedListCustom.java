public class LinkedListCustom {

    public Node insert(Node head, int val) {
        Node newNode = new Node(val);
        if(head == null) {
            head = newNode;
        } else {
            Node tmp = head;
            while(tmp != null && tmp.next != null) {
                tmp=tmp.next;
            }
            tmp.next = newNode;
        }
        return head;
    }

    public Node insertFirst(Node head, int val) {
        Node newNode = new Node(val);
        newNode.next = head;
        return newNode;
    }

    // insert after certain index (0-based index)
    public Node insertAt(Node head, int val, int index) {
        int count = 1;
        Node tmp = head;

        if(index == 0)
            return insertFirst(head, val);

        while(count < index) {
            count++;
            tmp=tmp.next;
        }
        Node newNode = new Node(val);
        newNode.next = tmp.next;
        tmp.next = newNode;
        return head;
    }

    public Node reverse(Node head) {
        Node curr = head;
        Node prev = null, next = null;
        while(curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }
        return prev;
    }

    public void print(Node head) {
        Node tmp = head;
        while(tmp != null) {
            System.out.print(tmp.val + " ");
            tmp=tmp.next;
        }
        System.out.println();
    }

    public Node delete(Node head, int val) {
        Node tmp = head;

        if(tmp != null && tmp.val == val) {
            head = head.next;
        }

        while(tmp != null && tmp.next != null) {
            if(tmp.next.val == val) {
                tmp.next = tmp.next.next;
            }
            tmp=tmp.next;
        }
        return head;
    }

    public boolean contains(Node head, int val) {
        Node tmp = head;
        while(tmp != null) {
            if(tmp.val == val)
                return true;
            tmp=tmp.next;
        }
        return false;
    }
}

class Node {
    int val;
    Node next;

    public Node() {}

    public Node(int val) { this.val = val; }
}
